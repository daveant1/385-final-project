# 385 Final Project

Final Project for ECE 385: Digital Systems Laboratory. Bootleg fighting game implemented using System Verilog for the game logic, PS/2 keyboard driver, and VGA monitor drivers, as well as C for the USB keyboard interface and Python for the image converter.
